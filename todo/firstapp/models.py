from django.db import models
from django.contrib.auth.models import User


class Task(models.Model):
    human = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
