from django.shortcuts import render, redirect
from .models import Task
from django.http import HttpResponseRedirect, HttpResponseNotFound
from .forms import CreateUserForm
from django.contrib.auth.models import User

from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required

def registerPage(request):
    if request.user.is_authenticated:
        return redirect('todo')
    else:
        form = CreateUserForm()
        if request.method == "POST":
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get("username")
                messages.success(request, "Account was created for " + user)

                return redirect('login')

        context = {'form': form}
        return render(request, "logup.html", context)


def loginPage(request):
    if request.user.is_authenticated:
        return redirect('todo')
    else:

        if request.method == "POST":
            username = request.POST.get("username")
            password = request.POST.get("password")

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('todo')
            else:
                messages.info(request, "Username or password is incorrect")
                return render(request, 'login.html')
        return render(request, "login.html")


def logoutUser(request):
    logout(request)
    return redirect('login')


@login_required(login_url='login')
def todo(request):
    human = User.objects.get(username=request.user)
    return render(request, "index.html", {"tasks": human.task_set.all()})


@login_required(login_url='login')
def create(request):
    if request.method == "POST":
        human = User.objects.get(username=request.user)
        human.task_set.create(name=request.POST.get("name"))
        print(human)
    return HttpResponseRedirect("/")


@login_required(login_url='login')
def edit(request, id):
    try:
        task = Task.objects.get(id=id)
        if request.method == "POST":
            task.name = request.POST.get("name")
            task.save()
            return HttpResponseRedirect("/")
        else:
            return render(request, "edit.html", {"task": task})   
    except Task.DoesNotExist:
        return HttpResponseNotFound("<h2>Task not found</h2>")


@login_required(login_url='login')
def delete(request, id):
    try:
        task = Task.objects.get(id=id)
        task.delete()
        return HttpResponseRedirect("/")
    except Task.DoesNotExist:
        return HttpResponseNotFound("<h2>Task not found</h2>")
