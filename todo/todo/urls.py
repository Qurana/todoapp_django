from django.contrib import admin
from django.urls import path
from firstapp import views

urlpatterns = [
    path('', views.todo, name='todo'),
    path('create/', views.create),
    path('admin/', admin.site.urls),
    path('delete/<int:id>/', views.delete),
    path('edit/<int:id>/', views.edit),
    path('register/', views.registerPage, name='register'),
    path('login/', views.loginPage, name='login'),
    path('logout/', views.logoutUser, name='logout')
]
